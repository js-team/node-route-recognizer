const commonjs = require("@rollup/plugin-commonjs");
const resolve = require("@rollup/plugin-node-resolve").nodeResolve;
const typescript = require("rollup-plugin-typescript");
const pkg = require("../package.json");

module.exports = [
  {
    input: "dist/lib/route-recognizer.js",
    output: [
      {
        name: "RouteRecognizer",
        file: pkg.main,
        format: "umd",
      },
      {
        file: pkg.module,
        format: "es",
      },
    ],
    plugins: [commonjs(), resolve({
      modulePaths: ['/usr/share/nodejs']
        }), typescript()],
  },
];
